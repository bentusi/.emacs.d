;;; coq-commenter-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "coq-commenter" "coq-commenter.el" (0 0 0 0))
;;; Generated autoloads from coq-commenter.el

(autoload 'coq-commenter-mode "coq-commenter" "\
Commenting support mode for coq proof assistant.

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "coq-commenter" '("coq-commenter-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; coq-commenter-autoloads.el ends here
