;;; -*- no-byte-compile: t -*-
(define-package "posframe" "20200408.135" "Pop a posframe (just a frame) at point" '((emacs "26")) :stars nil :commit "e62e5842682e2c0b021f0cbf5a0de25b8e2161e1" :keywords '("convenience" "tooltip") :authors '(("Feng Shu" . "tumashu@163.com")) :maintainer '("Feng Shu" . "tumashu@163.com") :url "https://github.com/tumashu/posframe")
