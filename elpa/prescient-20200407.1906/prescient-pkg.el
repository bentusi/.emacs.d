;;; -*- no-byte-compile: t -*-
(define-package "prescient" "20200407.1906" "Better sorting and filtering" '((emacs "25.1")) :stars nil :commit "3be69ac0d9d4e5dd5596673ce5dfebc31d8662ca" :keywords '("extensions") :authors '(("Radon Rosborough" . "radon.neon@gmail.com")) :maintainer '("Radon Rosborough" . "radon.neon@gmail.com") :url "https://github.com/raxod502/prescient.el")
