;;; -*- no-byte-compile: t -*-
(define-package "tldr" "20200330.1025" "tldr client for Emacs" '((emacs "24.3") (request "0.3.0")) :stars nil :commit "269bda7001613c0b70c0662d2a74d200765c1dcb" :keywords '("tools" "docs") :authors '(("Ono Hiroko" . "azazabc123@gmail.com")) :maintainer '("Ono Hiroko" . "azazabc123@gmail.com") :url "https://github.com/kuanyui/tldr.el")
