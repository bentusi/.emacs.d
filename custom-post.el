

(require 'init-misc)
(setq tabbar-ruler-global-tabbar t)    ; get tabbar
(setq tabbar-ruler-global-ruler t)     ; get global ruler
(setq tabbar-ruler-popup-menu t)       ; get popup menu.
(setq tabbar-ruler-popup-toolbar t)    ; get popup toolbar
(setq tabbar-ruler-popup-scrollbar t)  ; show scroll-bar on mouse-move
(require 'tabbar-ruler)

(require 'dired-single)

(turn-off-drag-stuff-mode)

(setq coq-use-pg t)
(setq coq-use-project-file t)
(setq coq-compile-before-require t)
